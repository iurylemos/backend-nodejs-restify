"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function crossOrigin(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    return next();
}
exports.crossOrigin = crossOrigin;
