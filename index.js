"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restify = require('restify')
const mongoose = require('mongoose')
const CORS_1 = require("../backend-nodejs-restify/common/CORS");
const corsmiddleware = require("restify-cors-middleware");
// var ObjectId = require('mongodb').ObjectID;
// const { MongoClient, ObjectId } = require('mongodb');
const Mongoose = require('mongoose')
// const ObjectId = Mongoose.Types.ObjectId;

//Método connect recebe uma conexão

mongoose.connect('mongodb+srv://iury:iury1996@passaro-api-5sw91.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(_ => {
        const cors = corsmiddleware({
            origins: ['*'],
            allowHeaders: ['*'],
            exposeHeaders: ["Authorization"]
        });
        //Só quero conectar ao restify se eu conseguir me conectar ao mongodb
        const server = restify.createServer({
            name: 'restify-passaro-urbano',
            version: '1.0.0',
            ignoreTrailingSlash: true
        })

        //Para receber as query
        server.use(CORS_1.crossOrigin);
        server.use(restify.plugins.queryParser())
        server.use(restify.plugins.bodyParser())
        server.pre(cors.preflight);
        server.use(cors.actual);


        const ofertasSchema = new mongoose.Schema({
            //Objeto que tem as propriedades do schema
            categoria: { type: String }, titulo: { type: String }, descricao_oferta: { type: String }, anunciante: { type: String }, valor: { type: String }, destaque: { type: String }, imagens: [{ url: { type: String } }], status: { type: String }, data: { type: String }
        })

        const comoUsarSchema = new mongoose.Schema({
            id_comousar: { type: String }, descricao: { type: String }
        })

        const ondeFicaSchema = new mongoose.Schema({
            id_ondefica: { type: String }, descricao: { type: String }
        })

        const pedidosSchema = new mongoose.Schema({
            status: { type: String }, email_cliente: { type: String }, endereco: { type: String }, numero: { type: String }, complemento: { type: String }, formaPagamento: { type: String }, valor: { type: Number }, data: { type: String }, itens: [{ id_oferta: { type: String }, img: { type: String }, titulo: { type: String }, descricao_oferta: { type: String }, valor: { type: String }, quantidade: { type: String } }],
        })

        const clientSchema = new mongoose.Schema({
            firstName: { type: String }, lastName: { type: String }, email: { type: String }, password: { type: String }
        })

        const Pedidos = mongoose.model('Pedidos', pedidosSchema)

        //Model da comoUsarSchema
        const ComoUsar = mongoose.model('ComoUsar', comoUsarSchema)

        //Criando o model
        //Automaticamente o mongoose cria uma coleção chamada Oferta
        const Oferta = mongoose.model('Oferta', ofertasSchema)

        const OndeFica = mongoose.model('OndeFica', ondeFicaSchema)

        const Client = mongoose.model('Client', clientSchema)


        // const ofertas = [
        //     {id: '1', categoria: 'restaurante', titulo: 'Super Burger', descricao_oferta: 'Rodízio de Mini-hambúrger com opção de entrada.', anunciante: 'Original Burger', valor: '29.9', destaque: 'true', imagens: [ { url: '/assets/ofertas/1/img1.jpg' }, { url: '/assets/ofertas/1/img2.jpg' }, { url: '/assets/ofertas/1/img3.jpg' }, { url: '/assets/ofertas/1/img4.jpg' } ]},
        //     {id: '2', categoria: 'restaurante', titulo: 'Cozinha Mexicana', descricao_oferta: 'Almoço ou Jantar com Rodízio Mexicano delicioso.', anunciante: 'Mexicana', valor: '32.9', destaque: 'true', imagens: [ { url: '/assets/ofertas/2/img1.jpg' }, { url: '/assets/ofertas/2/img2.jpg' }, { url: '/assets/ofertas/2/img3.jpg' }, { url: '/assets/ofertas/2/img4.jpg' } ]},
        //     {id: '3', categoria: 'restaurante', titulo: 'Pizzas Grandes', descricao_oferta: 'Pizza Grande, Mussarela, Marguerita ou outras!', anunciante: 'Nossa Pizza', valor: '21.9', destaque: 'false', imagens: [ { url: '/assets/ofertas/3/img1.jpg' }, { url: '/assets/ofertas/3/img2.jpg' }, { url: '/assets/ofertas/3/img3.jpg' }, { url: '/assets/ofertas/3/img4.jpg' } ]},
        //     {id: '4', categoria: 'diversao', titulo: 'Estância das águas', descricao_oferta: 'Diversão garantida com piscinas, trilhas e muito mais.', anunciante: 'Estância das águas', valor: '31.9', destaque: 'true', imagens: [ { url: '/assets/ofertas/4/img1.jpg' }, { url: '/assets/ofertas/4/img2.jpg' }, { url: '/assets/ofertas/4/img3.jpg' }, { url: '/assets/ofertas/4/img4.jpg' }, { url: '/assets/ofertas/4/img5.jpg' }, { url: '/assets/ofertas/4/img5.jpg' } ]},
        //     {id: '5', categoria: 'diversao', titulo: 'Kart', descricao_oferta: 'Bateria de Kart de 30 minutos.', anunciante: 'Speed Kart', valor: '54.9', destaque: 'false', imagens: [ { url: '/assets/ofertas/5/img1.jpg' }, { url: '/assets/ofertas/5/img2.jpg' } ]},
        //     {id: '6', categoria: 'diversao', titulo: 'Academia de tiro', descricao_oferta: 'Sessão de tiro dinâmico com Instrutor, Estande, Alvo e Equipamentos.', anunciante: 'Academia de Tiro', valor: '89.9', destaque: 'false', imagens: [ { url: '/assets/ofertas/6/img1.jpg' }, { url: '/assets/ofertas/6/img2.jpg' } ]},
        // ]

        // const arrayComoUsar = [
        //     { id: '1', descricao: 'Não é necessário agendar' },
        //     { id: '2', descricao: 'Válido de segunda a quinta das 12h as 23h' },
        //     { id: '3', descricao: 'Não é necessário agendar' },
        //     { id: '4', descricao: 'Necessário agendar com no mínimo 48 horas de antecedência' },
        //     { id: '5', descricao: 'Válido de segunda a sábado das 12h as 23h. Necessário agendamento prévio' },
        //     { id: '6', descricao: 'Não é necessário agendar. Sujeito à fila.' }
        // ]

        // const arrayOndeFica = [
        //     { id: '1', descricao: 'Avenida João da Silva, 255, São Paulo - SP' },
        //     { id: '2', descricao: 'Rua Francisco Mendes, 1000, Rio de Janeiro - SP' },
        //     { id: '3', descricao: 'Avenida Lúcio Rodrigues Alves, 33, Fortaleza - CE' },
        //     { id: '4', descricao: 'Avenida José de Oliveira, 550, Campo Grande - MS' },
        //     { id: '5', descricao: 'Avenida Júlia Abrão, 77, São Paulo - SP' },
        //     { id: '6', descricao: 'Estrada Maria das Graças, 12, Salvador - BA' },
        // ]

        server.get('/como-usar', (req, resp, next) => {
            ComoUsar.find().then(comoUsar => {
                console.log(comoUsar)

                const filteredId = comoUsar.filter(cs => cs.id_comousar === req.query.id_comousar)
                // console.log(filteredId)
                if (filteredId.length) {
                    resp.json(filteredId)
                } else if (comoUsar) {
                    resp.json(comoUsar)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })

        server.get('/onde-fica', (req, resp, next) => {
            OndeFica.find().then(ondeFica => {
                // console.log(ondeFica)
                const filteredId = ondeFica.filter(o => o.id_ondefica === req.query.id_ondefica)
                // console.log(filteredId)
                if (filteredId.length) {
                    resp.json(filteredId)
                } else if (ondeFica) {
                    resp.json(ondeFica)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })

        //Configurar uma URL e associar a uma função
        //Sempre que eu acessar essa url do browser, associando
        //a um determinado método HTTP, ele vai cair na configuração junto com o objeto server
        //e chamar uma função que associei com a rota.
        server.get('/ofertas', (req, resp, next) => {
            // o barra alunos traz a lista de alunos
            //o find me retorna todos os documentos dessa coleção
            Oferta.find().then(ofertas => {

                // console.log(ofertas)
                console.log('REQ QUERY', req.query)
                console.log('REQ QUERY ID,', req.query.id_oferta)
                const filteredDestaque = ofertas.filter(oferta => oferta.destaque === req.query.destaque)
                const filteredCategory = ofertas.filter(oferta => oferta.categoria === req.query.categoria)
                const filteredId = ofertas.filter(oferta => oferta._id == req.query.id_oferta)
                // console.log(ofertas.id)
                console.log('FILTERED Categoria', filteredCategory)
                console.log('Filttro por ID', filteredId)
                if (filteredCategory.length) {
                    //Filtro de categorias
                    resp.json(filteredCategory)
                } else if (filteredDestaque.length) {
                    //Filtro de destaque
                    resp.json(filteredDestaque)
                } else if (filteredId.length) {
                    //Filtro de ID
                    resp.json(filteredId)
                } else if (ofertas) {
                    resp.json(ofertas)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                // resp.json(ofertas)
                return next()
            })

        })

        server.get('/pedidos', (req, resp, next) => {


            Pedidos.find().then(pedidos => {
                const filteredEmail = pedidos.filter(pedido => pedido.email_cliente === req.query.email_cliente)

                console.log('filteredEmail', filteredEmail)

                if (filteredEmail.length) {
                    console.log('Filtro de categorias')
                    resp.json(filteredEmail)
                } else if (pedidos) {
                    resp.json(pedidos)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })




        server.get('/ofertas/:id_oferta', (req, resp, next) => {

            console.log('entrou aqui no /Id')
            Oferta.find().then(oferta => {
                if (oferta) {
                    resp.json(oferta)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })

        server.get('/cliente', (req, resp, next) => {
            console.log('Entrou na rota de pegar o cliente por Email')
            Client.find().then(clients => {
                const filteredEmail = clients.filter(cliente => cliente.email === req.query.email)
                console.log(clients)
                console.log('FilteredEmail', filteredEmail)
                console.log('FilteredEmail', filteredEmail.length)
                if (filteredEmail.length) {
                    resp.json(filteredEmail)
                    // } 
                    // else if (clients) {
                    //     resp.json(clients)
                } else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })


        /*server.get('/ofertas/:id/images/:ImageId', (req, resp, next) => {
            
            Oferta.findById(req.params.id).then(oferta => {
                if(oferta) {
                   let obj  = oferta.imagens.filter(o => o._id.toString() === req.params.ImageId.toString())
                    resp.json(obj)
                }else {
                    resp.status(404)
                    resp.json({ message: 'not found' })
                }
                return next()
            })
        })*/




        // server.get('/como-usar', (req, resp, next) => {
        //     //Filtrar o array baseado no valor do parãmetro
        //     //Filtrando o array na oferta que tiver o id, igual o id que estou passando no caminho da URL
        //     const filteredId = arrayComoUsar.filter(comoUsar => comoUsar.id === req.query.id)
        //     if (filteredId.length) {
        //         resp.json(filteredId)
        //     } else {
        //         //O que foi pedido na request não existe
        //         resp.status(404)
        //         resp.json({
        //             message: 'not found'
        //         })
        //         return next()
        //     }
        // })

        // server.get('/onde-fica', (req, resp, next) => {
        //     const filteredId = arrayOndeFica.filter(ondeFica => ondeFica.id === req.query.id)
        //     if (filteredId.length) {
        //         resp.json(filteredId)
        //     } else {
        //         //O que foi pedido na request não existe
        //         resp.status(404)
        //         resp.json({
        //             message: 'not found'
        //         })
        //         return next()
        //     }

        // })

        // server.get('/ofertas', (req, resp, next) => {
        //     const filtered = ofertas.filter(oferta => oferta.categoria === req.params.categoria)
        //     if(filtered.length) {
        //         resp.json("categoria: " + req.params.categoria)
        //          resp.json(filtered[0]) 
        //     }else {
        //         resp.status(404)
        //         resp.json({
        //             message: 'not found'
        //         })
        //         return next()
        //     }
        // })

        // Atualizar Pedido
        server.post('/atualizar-pedido', (req, resp, next) => {

            console.log('REQUISIÇÃO', req.body)
            console.log('OBJETO: _id', req.body._id)
            console.log('OBJETO A SER ATUALIZADO', req.body.pedido)
            console.log('STATUS A SER ATUALIZADO', req.body.pedido.status)

            let id = req.body._id

            let pedidos = new Pedidos(req.body)

            pedidos.updateOne({
                "_id": id,
                "status": req.body.pedido.status,
                "formaPagamento": req.body.pedido.formaPagamento,
                "endereco": req.body.pedido.endereco
            },

                function (err, _) {
                    if (err) {
                        if (err) return resp.send(err)
                    }
                    resp.json({
                        message: `update ${req.body._id}`
                    })
                }, (err) => {
                    console.log(err)
                });

        })


        //Atualizar Produto
        server.post('/atualizar-oferta', (req, resp, next) => {

            console.log('REQUISIÇÃO', req.body)
            console.log('OBJETO: _id', req.body._id)
            console.log('OBJETO A SER ATUALIZADO', req.body.produto)

            console.log('OBJETO DESTAQUE:', req.body.produto.destaque)

            let id = req.body._id

            let ofertas = new Oferta(req.body)

            ofertas.updateOne({
                "_id": id,
                "destaque": req.body.produto.destaque,
                "valor": req.body.produto.valor,
                "status": req.body.produto.status,
                "titulo": req.body.produto.titulo,
                "descricao_oferta": req.body.produto.descricao_oferta
            },

                function (err, _) {
                    if (err) {
                        if (err) return resp.send(err)
                    }
                    resp.json({
                        message: `update ${req.body._id}`
                    })
                }, (err) => {
                    console.log(err)
                });

        })


        server.post('/ofertas/:id', (req, resp, next) => {
            let oferta = new Oferta(req.body)
            // oferta.
            // oferta.collection.dele
        })

        //Remover Pedido
        server.post('/remover-pedido', (req, resp, next) => {


            let pedidos = new Pedidos(req.body)
            console.log('REQUISIÇÃO', req.body)
            console.log('OBJETO: _id', req.body._id)

            pedidos.remove({ _id: req.body._id }, function (err, _) {
                if (err) return resp.send(err)
                resp.json({
                    message: `deleted ${req.body._id}`
                })
            });
        })

        //Remover Oferta.
        server.post('/remover-oferta', (req, resp, next) => {


            let ofertas = new Oferta(req.body)
            console.log('REQUISIÇÃO', req.body)
            console.log('OBJETO: _id', req.body._id)

            ofertas.remove({ _id: req.body._id }, function (err, _) {
                if (err) return resp.send(err)
                resp.json({
                    message: `deleted ${req.body._id}`
                })
            });
        })



        //Utilizando o post
        server.post('/ofertas', (req, resp, next) => {

            console.log('ENTROU NO POST DE OFERTAS: ', req)
            //Criando um documento
            // let obj  = oferta.filter(p => p._id.toString())
            console.log(req.body)
            let oferta = new Oferta(req.body)
            oferta.save().then(oferta => {
                resp.json(oferta)
            }).catch(error => {
                resp.status(400)
                resp.json({ message: error.message })
            })
        })

        server.post('/como-usar', (req, resp, next) => {
            console.log(req.body)
            let comoUsar = new ComoUsar(req.body)
            comoUsar.save().then(comoUsar => {
                resp.json(comoUsar)
            }).catch(error => {
                resp.status(400)
                resp.json({ message: error.message })
            })
        })

        server.post('/onde-fica', (req, resp, next) => {
            let ondeFica = new OndeFica(req.body)
            ondeFica.save().then(ondeFica => {
                resp.json(ondeFica)
            }).catch(error => {
                resp.status(404)
                resp.json({ message: error.message })
            })
        })

        server.post('/pedidos', (req, resp, next) => {
            let pedidos = new Pedidos(req.body)
            console.log('PEDIDO:', req.body)
            console.log('PEDIDOS, ', pedidos)
            // console.log('OBJ', obj)
            pedidos.save().then(pedido => {
                // console.log('ID DO PEDIDO', pedido.id)
                resp.json(pedido.id)
            }).catch(error => {
                resp.status(404)
                resp.json({ message: error.message })
            })
        })

        server.post('/registrarCliente', (req, resp, next) => {
            let client = new Client(req.body)
            console.log('CLIENTE:', client)
            client.save().then(client => {

                const filteredEmail = clients.filter(cliente => cliente.email === req.query.email)
                console.log('FILTEREDEMAIL NO POST', filteredEmail)
                console.log(client)
                resp.json(client)
            }).catch(error => {
                resp.status(404)
                resp.json({ message: error.message })
            })
        })

        server.listen(5000, () => {
            console.log('API está rodando na porta 5000')
        })
    }).catch(console.error)

